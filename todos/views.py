from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def todolist(request):
    todo_list_list= TodoList.objects.all()
    context = {
        "todo_list": todo_list_list,
    }
    return render(request, "todos/todos.html", context)

def todolistdetail(request, id):
    todo_list_details = get_object_or_404(TodoList, id=id)
    context = {

        "todo_list_details": todo_list_details,

    }
    return render(request, "todos/todolistdetail.html", context)

def createtodolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id =list.id)
    else:
        form = TodoListForm()
    context = {
        "form":form,
    }
    return render(request, "todos/create.html", context)

def createtodoitem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id =item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form":form,
    }
    return render(request, "todos/createitem.html", context)

def edittodolist(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance = todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id = id)
    else:
        form = TodoListForm(instance = todolist)
    context = {
        "form": form,
        "todo_object": todolist,
    }
    return render(request, "todos/edit.html", context)

def edittodoitem(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance = todoitem)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id = item.list.id)
    else:
        form = TodoItemForm(instance = todoitem)
    context = {
        "form": form,
        "todo_object": todolist,
    }
    return render(request, "todos/edititem.html", context)

def deletetodolist(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")
