from django.urls import path
from todos.views import todolist, todolistdetail, createtodolist, edittodolist, deletetodolist, createtodoitem, edittodoitem

urlpatterns = [
    path("", todolist, name = "todo_list_list"),
    path("<int:id>/", todolistdetail, name = "todo_list_detail"),
    path("create/", createtodolist, name = "todo_list_create"),
    path("<int:id>/edit/", edittodolist , name = "todo_list_update"),
    path("<int:id>/delete/", deletetodolist , name = "todo_list_delete"),
    path("items/create/", createtodoitem, name = "todo_item_create"),
    path("items/<int:id>/edit/", edittodoitem, name = "todo_item_update"),
]
